CREATE TABLE IF NOT EXISTS users (
    id UUID NOT NULL PRIMARY KEY,
    username VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS todos (
    id UUID NOT NULL PRIMARY KEY,
    heading VARCHAR(100) NOT NULL,
    description TEXT NOT NULL,
    user_id UUID references users(id) NOT NULL,
    deadline_date timestamp NOT NULL
);