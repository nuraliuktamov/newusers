DELETE FROM casbin_rule WHERE v0='unauthorized' AND v1='/v1/user/register/' AND v2='POST';
DELETE FROM casbin_rule WHERE v0='unauthorized' AND v1='/v1/user/login/' AND v2='POST';
DELETE FROM casbin_rule WHERE v0='authorized' AND v1='/v1/user/profile/{id}/' AND v2='GET';
DELETE FROM casbin_rule WHERE v0='authorized' AND v1='/v1/todos/create//' AND v2='POST';
DELETE FROM casbin_rule WHERE v0='authorized' AND v1='/v1/todos/update//' AND v2='PUT';
DELETE FROM casbin_rule WHERE v0='authorized' AND v1='/v1/todos/info/{id}/' AND v2='GET';
DELETE FROM casbin_rule WHERE v0='authorized' AND v1='/v1/todos/user/' AND v2='GET';
DELETE FROM casbin_rule WHERE v0='authorized' AND v1='/v1/todos/deadline/' AND v2='POST';

