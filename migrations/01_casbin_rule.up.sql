CREATE TABLE IF NOT EXISTS  casbin_rule (
    p_type VARCHAR(100),
    v0 VARCHAR(100),
    v1 VARCHAR(100),
    v2 VARCHAR(100),
    v3 VARCHAR(100),
    v4 VARCHAR(100),
    v5 VARCHAR(100)
);

CREATE INDEX IF NOT EXISTS IDX_casbin_rule_p_type ON casbin_rule(p_type);
CREATE INDEX IF NOT EXISTS IDX_casbin_rule_v0 ON casbin_rule(v0);
CREATE INDEX IF NOT EXISTS IDX_casbin_rule_v1 ON casbin_rule(v1);
CREATE INDEX IF NOT EXISTS IDX_casbin_rule_v2 ON casbin_rule(v2);
CREATE INDEX IF NOT EXISTS IDX_casbin_rule_v3 ON casbin_rule(v3);
CREATE INDEX IF NOT EXISTS IDX_casbin_rule_v4 ON casbin_rule(v4);
CREATE INDEX IF NOT EXISTS IDX_casbin_rule_v5 ON casbin_rule(v5);

-- Rules for unauthorized users
-- swagger
INSERT INTO casbin_rule(p_type, v0, v1, v2) VALUES('p', 'unauthorized', '/swagger/*', 'GET') ON CONFLICT DO NOTHING;
INSERT INTO casbin_rule(p_type, v0, v1, v2) VALUES('p', 'authorized', '/swagger/*', 'GET') ON CONFLICT DO NOTHING;