INSERT INTO casbin_rule(p_type, v0, v1, v2) VALUES ('p', 'unauthorized', '/v1/user/register/', 'POST') ON CONFLICT DO NOTHING;
INSERT INTO casbin_rule(p_type, v0, v1, v2) VALUES ('p', 'unauthorized', '/v1/user/login/', 'POST') ON CONFLICT DO NOTHING;
INSERT INTO casbin_rule(p_type, v0, v1, v2) VALUES ('p', 'authorized', '/v1/user/profile/{id}/', 'GET') ON CONFLICT DO NOTHING;
INSERT INTO casbin_rule(p_type, v0, v1, v2) VALUES ('p', 'authorized', '/v1/todos/create/', 'POST') ON CONFLICT DO NOTHING;
INSERT INTO casbin_rule(p_type, v0, v1, v2) VALUES ('p', 'authorized', '/v1/todos/update/', 'PUT') ON CONFLICT DO NOTHING;
INSERT INTO casbin_rule(p_type, v0, v1, v2) VALUES ('p', 'authorized', '/v1/todos/info/{id}/', 'GET') ON CONFLICT DO NOTHING;
INSERT INTO casbin_rule(p_type, v0, v1, v2) VALUES ('p', 'authorized', '/v1/todos/user/', 'GET') ON CONFLICT DO NOTHING;
INSERT INTO casbin_rule(p_type, v0, v1, v2) VALUES ('p', 'authorized', '/v1/todos/deadline/', 'POST') ON CONFLICT DO NOTHING;