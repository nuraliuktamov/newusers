package repo

import (
	"errors"
)

//User ...
type User struct {
	ID       string `json:"id,omitempty"`
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

var (
	//ErrAlreadyExists ...
	ErrAlreadyExists = errors.New("Already exists")
)

//UserStorageI ...
type UserStorageI interface {
	Create(*User) error
	Update(*User) error
	GetUser(id string) (*User, error)
	CheckUniquess(username string) (isUsername bool, err error)
	GetUserByUsername(username string) (*User, error)
	IsUserExists(ID string) (bool, error)
}
