package postgres

import (
	// "database/sql"
	// "time"
	"database/sql"

	"github.com/jmoiron/sqlx"

	"github.com/google/uuid"
	_ "github.com/lib/pq" // for db driver
	"gitlab.com/todos/users/pkg/etc"
	"gitlab.com/todos/users/storage/repo"
)

type userRepo struct {
	db *sqlx.DB
}

// NewUserRepo ...
func NewUserRepo(db *sqlx.DB) repo.UserStorageI {
	return &userRepo{db: db}
}

func (cm *userRepo) Create(cl *repo.User) error {
	var (
		err error
	)
	id, err := uuid.NewRandom()
	if err != nil {
		return err
	}
	// Creating hash of a password
	hashedPassword, err := etc.GeneratePasswordHash(cl.Password)
	if err != nil {
		return err
	}
	insertNew :=
		`INSERT INTO
		users
		(
			id,
			username,
			password
		)
			VALUES  
			($1, $2, $3)`
	_, err = cm.db.Exec(
		insertNew,
		id.String(),
		cl.Username,
		string(hashedPassword),
	)
	if err != nil {
		return err
	}
	return nil
}

func (cm *userRepo) Update(cl *repo.User) error {
	updateClient := `
	UPDATE users 
	SET
		username = $1
	WHERE id = $2
	`
	_, err := cm.db.Exec(updateClient,
		cl.Username,
		cl.ID,
	)
	if err != nil {
		return err
	}
	return nil
}

func (cm *userRepo) GetUser(ID string) (*repo.User, error) {
	user := repo.User{}

	row := cm.db.QueryRow(`SELECT id, username from users WHERE id = $1`, ID)
	err := row.Scan(
		&user.ID,
		&user.Username,
	)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (cm *userRepo) CheckUniquess(username string) (isUsername bool, err error) {
	var exists int
	row := cm.db.QueryRow(`SELECT 1 FROM users where username = $1`, username)
	err = row.Scan(&exists)
	if err == sql.ErrNoRows {
		return false, nil
	}
	if err != nil {
		return false, err
	}
	if exists == 1 {
		return true, nil
	}
	return false, nil
}

func (cm *userRepo) GetUserByUsername(username string) (*repo.User, error) {
	user := repo.User{}

	row := cm.db.QueryRow(`SELECT id, username, password from users WHERE username = $1`, username)
	err := row.Scan(
		&user.ID,
		&user.Username,
		&user.Password,
	)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

func (cm *userRepo) IsUserExists(ID string) (isUsername bool, err error) {
	var exists int
	row := cm.db.QueryRow(`SELECT 1 FROM users where ID = $1`, ID)
	err = row.Scan(&exists)
	if err == sql.ErrNoRows {
		return false, nil
	}
	if err != nil {
		return false, err
	}
	if exists == 1 {
		return true, nil
	}
	return false, nil
}
