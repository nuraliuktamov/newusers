# workspace (GOPATH) configured at /go
FROM golang:1.15.8 as builder


#
RUN mkdir -p $GOPATH/src/gitlab.com/todos/users
WORKDIR $GOPATH/src/gitlab.com/todos/users

# Copy the local package files to the container's workspace.
COPY . ./

# installing depends and build
RUN export CGO_ENABLED=0 && \
    export GOOS=linux && \
    make build && \
    mv ./bin/users /



FROM alpine
COPY --from=builder users .
RUN mkdir config
COPY ./config/rbac_model.conf ./config/rbac_model.conf 
ENTRYPOINT ["/users"]