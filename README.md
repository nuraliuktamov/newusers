# todos USERS documentation

### Prerequisities:
- `go` with version above **1.14**
- `make` binary package
- `docker.io` (recommended)

To run the project, first you need to create `.env` file by running the following command:

    make create-env

Customize `.env` file according to your needs by replacing values for environment variables. Then run command:

    set -a &&. ./.env && set +a

To run the project in _development mode_:

    $ go run cmd/main.go

    or

    $ make run-dev

To run SQL migrations locally:

    make migrate-jeyran

To delete unnecessary branches locally, run:

    make delete-branches

To generate routes for swagger run:

    make swag-gen

Link for swagger of users:
    http://localhost:1235/swagger/index.html   
