package config

import (
	"os"
	"time"

	"github.com/spf13/cast"
)

// Config ...
type Config struct {
	Environment                string
	PostgresHost               string
	PostgresPort               int
	PostgresDatabase           string
	PostgresUser               string
	PostgresPassword           string
	HTTPPort                   string
	LogLevel                   string
	ServiceDir                 string
	AccessTokenDuration        time.Duration
	RefreshTokenDuration       time.Duration
	RefreshPasswdTokenDuration time.Duration

	CasbinConfigPath string

	// context timeout in seconds
	CtxTimeout int

	SigninKey string
}

// Load ...
func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))
	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(getOrReturnDefault("POSTGRES_PORT", 5434))
	c.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DB", "ucook_staging"))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "staging_user"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "12345"))
	c.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8000"))
	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))
	c.ServiceDir = cast.ToString(getOrReturnDefault("CURRENT_DIR", ""))
	c.CtxTimeout = cast.ToInt(getOrReturnDefault("CTX_TIMEOUT", 7))
	c.CasbinConfigPath = cast.ToString(getOrReturnDefault("CASBIN_CONFIG_PATH", "./config/rbac_model.conf"))
	c.SigninKey = cast.ToString(getOrReturnDefault("SIGNIN_KEY", "BsHZrCEQqhq0AHochcubd97hqDbZ5Wy8ypMcOEyNfRqpJLzCUc"))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}

	return defaultValue
}
