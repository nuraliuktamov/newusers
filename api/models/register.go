package models

//SendCodeResponse ...
type SendCodeResponse struct {
	ID string `json:"id"`
}

type DataUser struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginResponse struct {
	ID           string `json:"id"`
	Username     string `json:"username"`
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}
