package models

// GetProfileByJwtRequestModel ...
type GetProfileByJwtRequestModel struct {
	Token string `header:"Authorization"`
}
