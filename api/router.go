package api

import (
	casbin "github.com/casbin/casbin/v2"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/todos/users/api/docs"
	v1 "gitlab.com/todos/users/api/handlers/v1"
	"gitlab.com/todos/users/api/token"
	"gitlab.com/todos/users/config"
	"gitlab.com/todos/users/storage"
	"gitlab.com/todos/users/api/middleware"


	"gitlab.com/todos/users/pkg/logger"
)

// Config ...
type Config struct {
	Storage        storage.StorageI
	Logger         logger.Logger
	Cfg            config.Config
	CasbinEnforcer *casbin.Enforcer
}

// New is a constructor for gin.Engine
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(cfg Config) *gin.Engine {

	r := gin.New()

	r.Use(gin.Logger())

	jwtHandler := token.JWTHandler{
		SigninKey: cfg.Cfg.SigninKey,
	}

	r.Use(gin.Recovery())
	r.Use(middleware.NewAuthorizer(cfg.CasbinEnforcer, jwtHandler, cfg.Cfg))


	// HandlerV1 should be replaced with handlerV1
	HandlerV1 := v1.New(cfg.Storage, cfg.Logger, jwtHandler)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	groupV1 := r.Group("/v1")
	// users
	groupV1.POST("/user/register/", HandlerV1.RegisterUser)
	groupV1.POST("/user/login/", HandlerV1.Login)
	groupV1.GET("/user/profile/:id/", HandlerV1.Profile)

	return r
}
