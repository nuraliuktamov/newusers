package v1

import (
	"errors"
	"net/http"

	jwtg "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/todos/users/api/models"
	"gitlab.com/todos/users/api/token"
	"gitlab.com/todos/users/pkg/logger"
	"gitlab.com/todos/users/storage"
)

// HandlerV1 ..																																																																																																																																																																																										.
type HandlerV1 struct {
	storage    storage.StorageI
	log        logger.Logger
	jwtHandler token.JWTHandler
}

// New ...
func New(Storage storage.StorageI, log logger.Logger, jwtHandler token.JWTHandler) *HandlerV1 {
	return &HandlerV1{
		storage:    Storage,
		log:        log,
		jwtHandler: jwtHandler,
	}
}

const (
	//ErrorCodeInvalidURL ...
	ErrorCodeInvalidURL = "INVALID_URL"
	//ErrorCodeInvalidJSON ...
	ErrorCodeInvalidJSON = "INVALID_JSON"
	//ErrorCodeInvalidParams ...
	ErrorCodeInvalidParams = "INVALID_PARAMS"
	//ErrorCodeInternal ...
	ErrorCodeInternal = "INTERNAL"
	//ErrorCodeUnauthorized ...
	ErrorCodeUnauthorized = "UNAUTHORIZED"
	//ErrorCodeAlreadyExists ...
	ErrorCodeAlreadyExists = "ALREADY_EXISTS"
	//ErrorCodeNotFound ...
	ErrorCodeNotFound = "NOT_FOUND"
	//ErrorCodeInvalidCode ...
	ErrorCodeInvalidCode = "INVALID_CODE"
	//ErrorBadRequest ...
	ErrorBadRequest = "BAD_REQUEST"
	//ErrorCodeForbidden ...
	ErrorCodeForbidden = "FORBIDDEN"
	//ErrorCodeNotApproved ...
	ErrorCodeNotApproved = "NOT_APPROVED"
	//ErrorCodePasswordsNotEqual ...
	ErrorCodePasswordsNotEqual = "PASSWORDS_NOT_EQUAL"
	// ErrorExpectationFailed ...
	ErrorExpectationFailed = "EXPECTATION_FAILED"
	// ErrorUpgradeRequired ...
	ErrorUpgradeRequired = "UPGRADE_REQUIRED"
	// ErrorInvalidCredentials ...
	ErrorInvalidCredentials = "INVALID_CREDENTIALS"
)

var (
	// MySigningKey ...
	MySigningKey = []byte("secretphrase")
	// NewSigningKey ...
	NewSigningKey = []byte("FfLbN7pIEYe8@!EqrttOLiwa(H8)7Ddo")
)
// GetClaims ...
func GetClaims(h *HandlerV1, c *gin.Context) jwtg.MapClaims {
	var (
		ErrUnauthorized = errors.New("unauthorized")
		authorization   models.GetProfileByJwtRequestModel
		claims          jwtg.MapClaims
		err             error
	)

	authorization.Token = c.GetHeader("Authorization")
	if c.Request.Header.Get("Authorization") == "" {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeUnauthorized,
				Message: "Unauthorized request",
			},
		})
		h.log.Error("Unauthorized request: ", logger.Error(ErrUnauthorized))
		return nil
	}

	h.jwtHandler.Token = authorization.Token
	claims, err = h.jwtHandler.ExtractClaims()
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.InternalServerError{
				Status:  ErrorCodeUnauthorized,
				Message: "Unauthorized request",
			},
		})
		h.log.Error("Unauthorized request: ", logger.Error(err))
		return nil
	}
	return claims
}
