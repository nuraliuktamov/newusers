package v1

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/jsonpb"
	"gitlab.com/todos/users/api/models"
	"gitlab.com/todos/users/api/token"
	"gitlab.com/todos/users/storage/repo"
	"golang.org/x/crypto/bcrypt"
)

// RegisterUser ...
// @Summary Ragistration User
// @Description Ragistration User - API for registration user for setting user's todos
// @Tags register
// @Accept  json
// @Produce  json
// @Param contact body models.DataUser true "user"
// @Success 201
// @Failure 401
// @Router /v1/user/register/ [post]
func (h *HandlerV1) RegisterUser(c *gin.Context) {
	var (
		model    models.DataUser
		isUnique bool
	)

	err := c.ShouldBindJSON(&model)
	model.Username = strings.ToLower(model.Username)

	isUnique, err = h.storage.User().CheckUniquess(model.Username)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorBadRequest,
				Message: "Error while checking uniquess",
			},
		})
		return
	}
	if isUnique {
		c.JSON(http.StatusUnauthorized, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorCodeAlreadyExists,
				Message: "User with this username already exists",
			},
		})
		return
	} else {
		err = h.storage.User().Create(&repo.User{
			Username: model.Username,
			Password: model.Password,
		})

		c.JSON(http.StatusCreated, fmt.Sprintf("User %s successfully created!", model.Username))
	}
}

// Login ...
// @Summary Login for getting access token
// @Description Login for getting access token
// @Tags login
// @Accept  json
// @Produce  json
// @Param contact body models.DataUser true "user"
// @Success 201
// @Failure 401
// @Router /v1/user/login/ [post]
func (h *HandlerV1) Login(c *gin.Context) {
	var (
		model                     models.DataUser
		response                  models.LoginResponse
		accessToken, refreshToken string
		isMatch                   bool
	)

	err := c.ShouldBindJSON(&model)
	model.Username = strings.ToLower(model.Username)

	exists, err := h.storage.User().GetUserByUsername(model.Username)
	if exists == nil {
		if err != nil {
			c.JSON(http.StatusUnauthorized, models.ResponseError{
				Error: models.ServerError{
					Status:  ErrorCodeNotFound,
					Message: "There is no user with this username",
				},
			})
			return
		}
	} else {
		user, err := h.storage.User().GetUserByUsername(model.Username)
		err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(model.Password))
		if err != nil {
			isMatch = false
			c.JSON(http.StatusUnauthorized, models.ResponseError{
				Error: models.ServerError{
					Status:  ErrorBadRequest,
					Message: "Wrong password",
				},
			})

		} else {
			h.jwtHandler = token.JWTHandler{
				SigninKey: "BsHZrCEQqhq0AHochcubd97hqDbZ5Wy8ypMcOEyNfRqpJLzCUc",
				Sub:       user.ID,
				Iss:       "user",
				Role:      "authorized",
				Aud: []string{
					"todos",
				},
			}
			fmt.Println(isMatch)

			accessToken, refreshToken, err = h.jwtHandler.GenerateAuthJWT()
			if err != nil {
				c.JSON(http.StatusUnauthorized, models.ResponseError{
					Error: models.ServerError{
						Status:  ErrorCodeNotFound,
						Message: "Something went wrong while generating tokens",
					},
				})
				return
			}
			fmt.Println(response)
			response.AccessToken = accessToken
			response.RefreshToken = refreshToken
			response.ID = user.ID
			response.Username = user.Username

			c.JSON(http.StatusCreated, response)
			return
		}
	}
}

// Profile ...
// @Security ApiKeyAuth
// @Summary Profile for getting user profile
// @Description Profile for getting user profile
// @Tags profile
// @Accept  json
// @Produce  json
// @Param id path string true "User ID"
// @Success 201
// @Failure 401
// @Router /v1/user/profile/{id}/ [GET]
func (h *HandlerV1) Profile(c *gin.Context) {
	var (
		ID          string
		err         error
		jspbMarshal jsonpb.Marshaler
	)
	jspbMarshal.OrigName = true
	ID = c.Param("id")

	GetClaims(h, c)

	response, err := h.storage.User().GetUser(ID)
	if err != nil {
		c.JSON(http.StatusNotFound, models.ResponseError{
			Error: models.ServerError{
				Status:  ErrorCodeNotFound,
				Message: "There is no user with this user id",
			},
		})
		return
	} else {
		c.JSON(http.StatusCreated, response)
	}
}
